import React, { Component } from 'react'
import { Text, View,StyleSheet } from 'react-native'

export default class Contact extends Component {
  render() {
        const {containerStyle,textStyle} = myStyle
    return (
      <View style={containerStyle}>
        <Text style={textStyle}> Hello World! </Text>
      </View>
    )
  }
}

const myStyle = StyleSheet.create({
    containerStyle: {
        marginTop: 45
    },
    textStyle: {
        fontWeight: 'bold'
    }
})